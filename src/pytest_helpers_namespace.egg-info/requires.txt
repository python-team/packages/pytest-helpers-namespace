pytest>=6.0.0

[docs]
sphinx
sphinx-material-saltstack
sphinx-prompt
sphinxcontrib-spelling

[lint]
pylint==2.4.4
saltpylint==2019.6.7
pyenchant

[lint:python_version >= "3.7"]
black
reorder-python-imports

[tests]
